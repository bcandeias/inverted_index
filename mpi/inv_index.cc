#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>   
#include <cstring>   
#include <map>
#include <list>
#include <vector>
#include <algorithm>

#include "jsl_log.h"

// JSL_DBG_1    Critical
// JSL_DBG_2    Errors
// JSL_DBG_3    Information
// JSL_DBG_4    Debugging

// Header information
#define HEADER_ATTR 8
#define WARC_ "WARC/1.0"
#define WARC_TYPE "WARC-Type"
#define WARC_INFO_TYPE "warcinfo"
#define WARC_URI "WARC-Target-URI"
#define WARC_CLENGTH "Content-Length"

// Parse
#define MIN_WORD_LEN 4
#define ALLOWED_CHARS "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"   //no numbers
#define DELIMITERS " ,.!?#\""

// Paths
#define SOURCE_FILE_PATH "/tmp/mpi"
#define OUTPUT_DIR "./output/"


// Creates a number from a string, by simply shifting the bits
unsigned int s2i(std::string n)
{
  std::istringstream ist(n);
  unsigned int num;
  ist >> num;
  return num;
}

std::string
i2s(int i)
{
  std::ostringstream ost;
  ost << i;
  return ost.str();
}

bool valid_char(char c)
{
    return !(isalpha(c));
}

bool isvalid(std::string s)
{
  return (s.find_first_not_of(ALLOWED_CHARS) == std::string::npos);
}


////////////////////////////////////////////////////////////////////////////////////



void
flush_results(int worker_id, std::map<std::string, std::map<std::string, int> > &rev_index)
{
    jsl_log(JSL_DBG_3,">>> Flushing results for worker %i\n", worker_id);

    std::string wid = i2s(worker_id);
    std::string fileName = OUTPUT_DIR;
    fileName.append("worker_");
    fileName.append(wid);
    fileName.append(".txt");
    
    std::ofstream out_file;
    out_file.open(fileName.c_str());

    jsl_log(JSL_DBG_4,"\twritting to file %s\n", fileName.c_str());
    printf("[Worker] %d Map Size: %d\n", worker_id, rev_index.size());

    std::map<std::string, std::map<std::string, int> >::iterator map_it = rev_index.begin();

    std::string content=""; 
    std::map<std::string, int>::iterator url_it;

    for(; map_it != rev_index.end(); map_it++)
    {
        content += map_it->first;
        content.append("\t");
        content.append(i2s( (map_it->second).size() ));
        content.append("\n");

        url_it = (map_it->second).begin();
        
        for(; url_it !=  (map_it->second).end(); url_it++)
        {
            content.append(url_it->first);
            content.append("\t");
            content.append(i2s(url_it->second));
            content.append("\n");
        }

        content.append("\n");

        out_file.write(content.c_str(), content.size());
        content = "";
    }


    out_file.close();
    jsl_log(JSL_DBG_3,"<<< Flushing results for worker %i\n", worker_id);
}

// Moves the Input File Stream to the the next document by consuming
// all the lines until it encounters "WARC/1.0"
void move_caret(std::ifstream &file, int &r)
{
    jsl_log(JSL_DBG_3,">>> Entering move_caret() function\n");

    std::string s;
    bool found = false;
    
    while (!found)
    {
        if(file.eof())
        {
            jsl_log(JSL_DBG_1,"\tCaught EOF\n");
            r = -1;
            return;
        }

        getline(file, s);
        if(s.compare(WARC_) == 0)
            found = true;            
    }
    r = 0;

    jsl_log(JSL_DBG_3,"<<< Exiting move_caret() function\n");
}

// Iterates the header to fill cur_url and doc_length
bool handle_header(std::ifstream &file, std::string &cur_url, unsigned int &doc_length)
{
    jsl_log(JSL_DBG_3,">>> Entering handle_header() function\n");

    std::string s;
    std::vector<std::string> splitted_string;
    bool success = false;

    for(int i = 0; i < HEADER_ATTR ; i++)
    {
        getline(file, s);

        std::size_t delim_pos = s.find(' ');
        std::string tag = s.substr(0, delim_pos-1);
        std::string value = s.substr(delim_pos + 1);

        if(tag.compare(WARC_TYPE) == 0)
            if(value.compare(WARC_INFO_TYPE) == 0) 
                goto release;
        
        if(tag.compare(WARC_URI) == 0)
            cur_url = value; //ignore whitespace after ':'
        
        else if(tag.compare(WARC_CLENGTH) == 0)
            doc_length = s2i(value);
        
    }

    success = true;
    
    jsl_log(JSL_DBG_4,"\theader handled... url: %s | doc_length: %u \n",cur_url.c_str(), doc_length );
    
    release:

    if (success)
        jsl_log(JSL_DBG_3,"<<< Exiting handle_header() function \tSUCCESS\n");
    else
        jsl_log(JSL_DBG_3,"<<< Exiting handle_header() function \tFAIL\n");

    return success;
}             

void parse_content(std::string line, std::string cur_url, std::map<std::string, std::map<std::string, int> > &rev_index)
{
    jsl_log(JSL_DBG_3,">>> Entering parse_content() function\n");

    size_t current;
    size_t next = -1;

    do {
        current = next + 1;
        next = line.find_first_of( DELIMITERS, current );
        std::string word = line.substr( current, next - current );

        if(word.size() < MIN_WORD_LEN || !isvalid(word))
            continue;


        // jsl_log(JSL_DBG_4,"\"%s\"\n", word.c_str());

        std::map<std::string, std::map<std::string, int> >::iterator map_it = rev_index.find(word);
        
        if(map_it == rev_index.end())   // not in the map
        {
            jsl_log(JSL_DBG_4,"\tword %s not in the map\n", word.c_str());
            std::map<std::string, int> url_map;
            url_map[cur_url] = 1;
            rev_index[word] = url_map;
        }
        else // word found in rev_index        
            (map_it->second)[cur_url]++;  // if url does not exist, initializes one entry with that URL

    } while (next != std::string::npos);

    jsl_log(JSL_DBG_3,"<<< Exiting parse_content() function\n");
}

// reads the whole document
void read_document(std::ifstream &file, std::map<std::string, std::map<std::string, int> > &rev_index, int &r)
{
    jsl_log(JSL_DBG_3, ">>> Entering read_document() function\n");

    std::string line, cur_url;
    unsigned int doc_length, bytes_read;


     // check if we landed on a document or not
    getline (file, line);
    if(line.compare(WARC_) != 0) // is not WARC/1.0
    {
        jsl_log(JSL_DBG_4,"\tlanded on line:\n\t%s\n", line.c_str());   
        move_caret(file, r); // move to the begin of the next document
        jsl_log(JSL_DBG_4,"\tcaret moved\n");
        if(r != 0) return;
    } 
            

    // handle header
    if( !handle_header(file, cur_url, doc_length) ) // if false, this document should be ignored (invalid header)
    {
        jsl_log(JSL_DBG_2, "\tinvalid header! ignoring current document...\n");
        return;
    }

    jsl_log(JSL_DBG_4,"\tignoring empty line after header\n");
    getline (file, line); // consume empty line after the header
 
    jsl_log(JSL_DBG_4,"\tdocument URL: %s \n",cur_url.c_str());
    jsl_log(JSL_DBG_4,"\tdocument payload: %u \n",doc_length);

    int old_pos = (int) file.tellg();
    jsl_log(JSL_DBG_4,"\tcurrent document position: %d \n",old_pos);

    char * buffer = new char [doc_length];
    file.read(buffer, doc_length);
    std::string document(buffer, doc_length);

    int new_pos = (int) file.tellg();
    jsl_log(JSL_DBG_4,"\tcurrent document position: %d  , %u bytes read\n", new_pos - old_pos, doc_length);

    // parse the document's content
    parse_content(document, cur_url, rev_index);

    jsl_log(JSL_DBG_4,"\tignoring empty line after document\n");
    getline (file, line); // consume empty line
    
    jsl_log(JSL_DBG_4,"\tcurrent byte: %d \n", (int) file.tellg());

    jsl_log(JSL_DBG_3,"<<< Exiting read_document() function\n");
}

// Do worker stuff
void analyze(int worker_id, int total_workers)
{
    jsl_log(JSL_DBG_3,">>> Entering worker() function\n");

    // map for reverse search, where <key, value> is <word , list of URL>
    std::map< std::string, std::map< std::string, int> > rev_index;
  
    std::string file_name = SOURCE_FILE_PATH;
    file_name.append("/CC-MAIN-20140707234000-00000-ip-10-180-212-248.ec2.internal.warc.wet");

    std::string line;
    unsigned long offset, offset_beg, offset_end;
    std::streampos begin, end;
    
    // open file
    std::ifstream file (file_name.c_str());

    //  read
    if (file.is_open())
    {
        // determine file size
        begin = file.tellg();
        file.seekg (0, std::ios::end);
        end = file.tellg();
        unsigned long file_size = end - begin;

        // determine where to start reading
        offset = file_size / total_workers;
        offset_beg = (worker_id - 1) * offset;
        offset_end = worker_id * offset;
        jsl_log(JSL_DBG_4,"will read from byte %lu to byte %lu\n", offset_beg, offset_end);

        // go to the correct position in the file
        file.seekg (offset_beg, std::ios::beg);
        
        int r = 0;;
        do
        {
            jsl_log(JSL_DBG_4, "\tdocument found...\n");
            read_document(file, rev_index, r); // a document is the lowest data unit
            jsl_log(JSL_DBG_3,"document read: current byte %lu\toffset_end %lu\tmap size: %d\n", (unsigned long) file.tellg(), offset_end, rev_index.size());
        } 
        while ( file.tellg() > 0 && file.tellg() < offset_end && r == 0); // if it passes offset_end, is because it read the file until its end
        
        jsl_log(JSL_DBG_3,"Flushing results...\n");
        flush_results(worker_id, rev_index);

        file.close();
    }

    else printf("Unable to open file\n"); 

    jsl_log(JSL_DBG_3,"<<< Exiting worker() function\n");
}




void read_worker_file(std::string file_name, std::map<std::string, std::map<std::string, int> > &inv_index)
{

    jsl_log(JSL_DBG_3,">>> Entering read_worker_file() function\n");     

    std::ifstream file (file_name.c_str());

    if (file.is_open())
    {
        std::string line;

        while(getline(file, line))
        {
            std::size_t delim_pos = line.find('\t');
            std::string word = line.substr(0, delim_pos);
            int urls_num = s2i(line.substr(delim_pos + 1));

            jsl_log(JSL_DBG_4,"\tword: %s  occurences: %d\n", word.c_str(), urls_num);     
            
            for(int i = 0; i < urls_num; i++)
            {
                getline(file, line);
                delim_pos = line.find('\t');
                std::string url = line.substr(0, delim_pos);
                int occurences = s2i(line.substr(delim_pos + 1));

                (inv_index[word])[url] = occurences;
            }

            jsl_log(JSL_DBG_4,"\tinv_index size: %d\n",inv_index.size());     

            getline(file,line); // consume empty line
        }

        file.close();
    }


    jsl_log(JSL_DBG_3,"<<< Exiting read_worker_file() function\n");
}

/*
void
flush_results(int worker_id, std::map<std::string, std::map<std::string, int> > &rev_index)
{
    jsl_log(JSL_DBG_3,">>> Flushing results for worker %i\n", worker_id);

    std::string wid = i2s(worker_id);
    std::string fileName = OUTPUT_DIR;
    fileName.append("master.txt");
    
    std::ofstream myfile;
    myfile.open(fileName.c_str());

    jsl_log(JSL_DBG_4,"\twritting to file %s\n", fileName.c_str());
    jsl_log(JSL_DBG_4,"[Master] Map Size: %d\n", rev_index.size());

    std::map<std::string, std::map<std::string, int> >::iterator map_it = rev_index.begin();

    for(; map_it != rev_index.end(); map_it++)
    {
        std::string word = map_it->first;

        word.append("\t");
        word.append(i2s(map_it->second.size()));
        word.append("\n");

        myfile.write(word.c_str(), word.size());

        std::map<std::string, int>::iterator url_it = (map_it->second).begin();
        
        for(; url_it !=  (map_it->second).end(); url_it++)
        {
            std::string new_link = url_it->first;
            int reps = url_it->second;

            new_link.append("\t");
            new_link.append(i2s(reps));
            new_link.append("\n");
            myfile.write(new_link.c_str(), new_link.size());
        }

        myfile.write("\n", 1);
    }

    myfile.close();
    jsl_log(JSL_DBG_3,"<<< Flushing results for worker %i\n", worker_id);
}
*/

// Do master stuff
void collect(int total_workers)
{
    jsl_log(JSL_DBG_3,">>> Entering master() function\n");     

    std::string file_name;
    std::map<std::string, std::map<std::string, int> > rev_index;

    for (int i = 1; i <= total_workers ; i++)
    {
        file_name = OUTPUT_DIR;
        file_name.append("worker_");
        file_name.append(i2s(i));
        file_name.append(".txt");

        jsl_log(JSL_DBG_4,"\treading file from worker %d\n", i);     

        read_worker_file(file_name, rev_index);
    }

    flush_results(0, rev_index);

    printf("Map size: %d\n", rev_index.size());

    jsl_log(JSL_DBG_3,"<<< Exiting master() function\n");
}


int main(int argc, char *argv[])
{
    jsl_log(JSL_DBG_3,">>> Entering main() function\n");
    
    jsl_set_debug(0);

    // check
    if (argc != 3)
    {
        printf("usage: ./inv_index [argument]\n");
        exit(0);
    }

    int total_workers = s2i(argv[1]);
    int worler_id = s2i(argv[2]);

    printf("Launching a Analyze...\n");
    analyze(worler_id, total_workers);
    printf("Launching a Collect...\n");
    collect(total_workers);


    jsl_log(JSL_DBG_3,"<<< Exiting main() function\n");
    return 0;
}


